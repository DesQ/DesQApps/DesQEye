project(
    'DesQ Eye',
    'c',
	'cpp',
	version: '0.0.9',
	license: 'GPLv3',
	meson_version: '>=0.59.0',
	default_options: [
		'cpp_std=c++17',
		'c_std=c11',
		'warning_level=2',
		'werror=false',
	],
)

add_global_arguments( '-DPROJECT_VERSION="v@0@"'.format( meson.project_version() ), language : 'cpp' )
add_project_link_arguments( ['-rdynamic','-fPIC'], language:'cpp' )

GlobalInc = include_directories( '.' )

# If use_qt_version is 'auto', we will first search for Qt6.
# If all Qt6 packages are available, we'll use it. Otherwise,
# we'll switch to Qt5.
if get_option('use_qt_version') == 'auto'
	# Check if Qt6 is available
	QtDeps = dependency(
		'qt6',
		modules: [ 'Core', 'Gui', 'Widgets', 'OpenGLWidgets' ],
		required: false
	)

	WayQt       = dependency( 'wayqt-qt6', required: false )

	DesQCore    = dependency( 'desq-core-qt6', required: false )
	DesQGui     = dependency( 'desq-gui-qt6', required: false )
	DesQWidgets = dependency( 'desq-widgets-qt6', required: false )

    DFApp       = dependency( 'df6application', required: false )
    DFInotify   = dependency( 'df6inotify', required: false )
    DFSettings  = dependency( 'df6settings', required: false )
	DFUtils     = dependency( 'df6utils', required: false )
	DFXdg       = dependency( 'df6xdg', required: false )

	Qt6_is_Found = QtDeps.found() and WayQt.found() and DesQCore.found() and DesQGui.found() and DesQWidgets.found()
    Qt6_is_Found = Qt6_is_Found and DFApp.found() and DFSettings.found() and DFUtils.found() and DFXdg.found()

	if Qt6_is_Found
		Qt = import( 'qt6' )
		message( 'Using Qt6' )

	else
		Qt = import( 'qt5' )
		QtDeps = dependency(
			'qt5',
			modules: [ 'Core', 'Gui', 'Widgets' ],
		)

		WayQt       = dependency( 'wayqt' )

		DesQCore    = dependency( 'desq-core' )
		DesQGui     = dependency( 'desq-gui' )
		DesQWidgets = dependency( 'desq-widgets' )

        DFApp       = dependency( 'df5application' )
        DFInotify   = dependency( 'df5inotify' )
        DFSettings  = dependency( 'df5settings' )
		DFUtils     = dependency( 'df5utils' )
		DFXdg       = dependency( 'df5xdg' )
	endif

# User specifically wants to user Qt5
elif get_option('use_qt_version') == 'qt5'
	Qt = import( 'qt5' )

	QtDeps = dependency(
		'qt5',
		modules: [ 'Core', 'Gui', 'Widgets' ],
		required: true
	)

	WayQt       = dependency( 'wayqt' )

	DesQCore    = dependency( 'desq-core' )
	DesQGui     = dependency( 'desq-gui' )
	DesQWidgets = dependency( 'desq-widgets' )

    DFApp       = dependency( 'df5application' )
    DFInotify   = dependency( 'df5inotify' )
    DFSettings  = dependency( 'df5settings' )
	DFUtils     = dependency( 'df5utils' )
	DFXdg       = dependency( 'df5xdg' )

# User specifically wants to user Qt6
elif get_option('use_qt_version') == 'qt6'
	Qt = import( 'qt6' )

	QtDeps = dependency(
		'qt6',
		modules: [ 'Core', 'Gui', 'Widgets', 'OpenGLWidgets' ],
	)

	WayQt       = dependency( 'wayqt-qt6' )

	DesQCore    = dependency( 'desq-core-qt6' )
	DesQGui     = dependency( 'desq-gui-qt6' )
	DesQWidgets = dependency( 'desq-widgets-qt6' )

    DFApp       = dependency( 'df6application' )
    DFInotify   = dependency( 'df6inotify' )
    DFSettings  = dependency( 'df6settings' )
	DFUtils     = dependency( 'df6utils' )
	DFXdg       = dependency( 'df6xdg' )
endif

# ArchiveQt5
# OpenCV = dependency( 'opencv4' )
Exiv  = dependency( 'exiv2' )
ReSVG = meson.get_compiler( 'cpp' ).find_library( 'resvg' )

Deps = [ QtDeps, DesQCore, DesQGui, DesQWidgets, Exiv, ReSVG, DFSettings, DFInotify, DFApp, DFUtils, DFXdg ]

Includes = [
    'ImageLib',
  	'ThumbsView',
  	'UI'
]

Headers = [
    'ImageLib/ImageProps.hpp',
    'UI/DesQEye.hpp',
  	'UI/DesQEyeView.hpp',
  	'UI/Widgets.hpp',
  	'ThumbsView/Thumbnailer.hpp',
  	'ThumbsView/ThumbsView.hpp',
  	'ThumbsView/ThumbsModel.hpp'
]

Sources = [
    'Main.cpp',
    'ImageLib/ImageProps.cpp',
  	'UI/DesQEye.cpp',
  	'UI/DesQEyeView.cpp',
  	'UI/Widgets.cpp',
  	'ThumbsView/Thumbnailer.cpp',
  	'ThumbsView/ThumbsView.cpp',
  	'ThumbsView/ThumbsModel.cpp'
]

Mocs = Qt.compile_moc(
	headers : Headers,
	dependencies: QtDeps
)

Resources = Qt.compile_resources(
	name: 'Eye_rcc',
	sources: 'icons/icons.qrc'
)

desqeye = executable(
    'desq-eye', [ Sources, Mocs, Resources ],
    dependencies: Deps,
	include_directories: [ Includes ],
    install: true
)

conf_data = configuration_data()
conf_data.set( 'version', meson.project_version() )
conf_data.set( 'INSTALL_PREFIX', get_option( 'prefix' ) )

configure_file(
	input : 'desq-eye.desktop.in',
    output : 'desq-eye.desktop',
	install: true,
    install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'applications' ),
    configuration : conf_data
)

install_data(
	'Eye.conf',
	install_dir: join_paths( get_option( 'prefix' ), get_option( 'datadir' ), 'desq', 'configs' )
)

install_data(
    'README.md', 'Changelog', 'ReleaseNotes',
    install_dir: join_paths( get_option( 'datadir' ), 'desq-Eye' ),
)

install_data(
    'icons/desq-eye.svg',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', 'scalable', 'apps' ),
)

install_data(
    'icons/desq-eye.png',
    install_dir: join_paths( get_option( 'datadir' ), 'icons', 'hicolor', '256x256', 'apps' ),
)

summary = [
	'',
	'--------------',
	'DesQ Eye @0@'.format( meson.project_version() ),
	'--------------',
	''
]
message( '\n'.join( summary ) )
