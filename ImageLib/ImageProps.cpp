/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ImageProps.hpp"

static QMimeDatabase mimeDb;

DesQ::Eye::ImageProps::ImageProps( QString filename ) {
    QFileInfo    info( filename );
    QImageReader ir( filename );
    QMimeType    mType = mimeDb.mimeTypeForFile( filename );

    mImageFileName = info.fileName();
    mImagePath     = info.absolutePath();
    mImageSize     = QString( "%1 x %2" ).arg( ir.size().width() ).arg( ir.size().height() );
    mImageFormat   = QString( "%1 (%2)" ).arg( mType.comment() ).arg( mType.name() );
}


QString DesQ::Eye::ImageProps::imageFileName() {
    return mImageFileName;
}


QString DesQ::Eye::ImageProps::imagePath() {
    return mImagePath;
}


QString DesQ::Eye::ImageProps::imageSize() {
    return mImageSize;
}


QString DesQ::Eye::ImageProps::imageFormat() {
    return mImageFormat;
}
