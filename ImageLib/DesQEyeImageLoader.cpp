/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

/**
 * This file contains code to load images using OpenCV. Most of the code
 * is pieced together from various sources in the internet.
 * Two main functions in this file are
 *  1. QImage loadImage(...);
 *  2. QImage loadThumb(...);
 *
 * loadImage(...) function load the image using OpenCV, aligned properly
 * based on EXIF data, etc, etc.. The loaded image is then converted to
 * QImage and returned.
 *
 * loadThumb(...) function loads the thumbnail os an image using OpenCV
 * The loaded image is then converted to QImage and returned.
 */
