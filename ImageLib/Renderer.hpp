/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "RenderOptions.hpp"

class RenderTask;

namespace DesQEye {
    class Renderer;

    enum RenderOptions {
        Rotate0,
        Rotate90,
        Rotate180,
        Rotate270
    };
}

class DesQEye::Renderer : public QObject {
    Q_OBJECT;

    public:
        Renderer( QObject *parent = nullptr );

        void setImage( QString path );
        QImage acquire( QSize imgSz, DesQEye::RenderOptions opts );

        void reload();

    private:
        QString mPath;
        QImage *mImg;
        qint64 validFrom = -1;

        void validateImage( int pg, QImage img, qint64 id );

        /**
         * Cache to store the properly scaled image for given size
         */
        QHash<QSize, QImage> sizeCache;
        QVector<QSize> sizes;
        int sizeCacheLimit = 20;

        /**
         * Temporally sorted requests
         * We take the latest request and process it first
         */
        QHash<qint64, RenderTask *> requestCache;
        QList<qint64> requests;

    Q_SIGNALS:
        void pageRendered( int );
};
