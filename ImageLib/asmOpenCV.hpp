/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

/**
 * Functions to convert between OpenCV's cv::Mat and Qt's QImage and QPixmap.
 *
 * Andy Maloney <asmaloney@gmail.com>
 * https://asmaloney.com/2013/11/code/converting-between-cvmat-and-qimage-or-qpixmap
 **/

#include <QDebug>
#include <QImage>
#include <QPixmap>
#include <QtGlobal>

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/types_c.h"

/*
 * Endianness
 * ---
 *
 * Although not totally clear from the docs, some of QImage's formats we use here are
 * endian-dependent. For example:
 *
 *    Little Endian
 *       QImage::Format_ARGB32 the bytes are ordered:    B G R A
 *       QImage::Format_RGB32 the bytes are ordered:     B G R (255)
 *       QImage::Format_RGB888 the bytes are ordered:    R G B
 *
 *    Big Endian
 *       QImage::Format_ARGB32 the bytes are ordered:    A R G B
 *       QImage::Format_RGB32 the bytes are ordered:     (255) R G B
 *       QImage::Format_RGB888 the bytes are ordered:    R G B
 *
 * Notice that Format_RGB888 is the same regardless of endianness. Since OpenCV
 * expects (B G R) we need to swap the channels for this format.
 *
 * This is why some conversions here swap red and blue and others do not.
 *
 * This code assumes little endian. It would be possible to add conversions for
 * big endian machines though. If you are using such a machine, please feel free
 * to submit a pull request on the GitHub page.
 */
#if Q_BYTE_ORDER == Q_BIG_ENDIAN
#error Some of QImage's formats are endian-dependant. This file assumes little endian. See comment at top of header.
#endif

namespace ASM {
    // TODO: cover all cases - should we try to cover all cases?
    inline QImage cvMatToQImage( const cv::Mat& inMat ) {
        switch ( inMat.type() ) {
            // 8-bit, 4 channel
            case CV_8UC4: {
                QImage image( inMat.data,
                              inMat.cols, inMat.rows,
                              static_cast<int>(inMat.step),
                              QImage::Format_ARGB32 );

                return image;
            }

            // 8-bit, 3 channel
            case CV_8UC3: {
                QImage image( inMat.data,
                              inMat.cols, inMat.rows,
                              static_cast<int>(inMat.step),
                              QImage::Format_RGB888 );

                return image.rgbSwapped();
            }

            // 8-bit, 1 channel
            case CV_8UC1: {
                QImage image( inMat.data,
                              inMat.cols, inMat.rows,
                              static_cast<int>(inMat.step),
                              QImage::Format_Grayscale8 );

                return image;
            }

            default: {
                qWarning() << "ASM::cvMatToQImage() - cv::Mat image type not handled in switch:" << inMat.type();
                break;
            }
        }

        return QImage();
    }


    // TODO: cover all cases - should we try to cover all cases?
    inline cv::Mat QImageToCvMat( const QImage& inImage ) {
        switch ( inImage.format() ) {
            // 8-bit, 4 channel
            case QImage::Format_ARGB32:
            case QImage::Format_ARGB32_Premultiplied: {
                cv::Mat mat( inImage.height(), inImage.width(),
                             CV_8UC4,
                             const_cast<uchar *>(inImage.bits() ),
                             static_cast<size_t>(inImage.bytesPerLine() )
                );

                return mat.clone();
            }

            // 8-bit, 3 channel
            case QImage::Format_RGB32: {
                cv::Mat mat( inImage.height(), inImage.width(),
                             CV_8UC4,
                             const_cast<uchar *>(inImage.bits() ),
                             static_cast<size_t>(inImage.bytesPerLine() )
                );

                cv::Mat matNoAlpha;
                cv::cvtColor( mat, matNoAlpha, cv::COLOR_BGRA2BGR ); // drop the all-white alpha channel

                return matNoAlpha;
            }

            // 8-bit, 3 channel
            case QImage::Format_RGB888: {
                QImage swapped = inImage.rgbSwapped();
                return cv::Mat( swapped.height(), swapped.width(),
                                CV_8UC3,
                                const_cast<uchar *>(swapped.bits() ),
                                static_cast<size_t>(swapped.bytesPerLine() )
                ).clone();
            }

            // 8-bit, 1 channel
            case QImage::Format_Indexed8: {
                cv::Mat mat( inImage.height(), inImage.width(),
                             CV_8UC1,
                             const_cast<uchar *>(inImage.bits() ),
                             static_cast<size_t>(inImage.bytesPerLine() )
                );

                return mat.clone();
            }

            default: {
                qWarning() << "ASM::QImageToCvMat() - QImage format not handled in switch:" << inImage.format();
                break;
            }
        }

        return cv::Mat();
    }
}
