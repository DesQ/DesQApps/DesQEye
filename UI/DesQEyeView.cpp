/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include <desq/FSWatcher.hpp>

#include <QOpenGLWidget>

#include "DesQEyeView.hpp"
#include "Widgets.hpp"
#include "Global.hpp"

DesQ::Eye::View::View( QWidget *parent ) : QGraphicsView( parent ) {
    /** Use OpenGL if the user desires */
    if ( eyeSett->value( "UseOpenGL" ) ) {
        setViewport( new QOpenGLWidget() );
    }

    setRenderHints( QPainter::Antialiasing | QPainter::TextAntialiasing );
    setCacheMode( QGraphicsView::CacheBackground );
    setViewportUpdateMode( QGraphicsView::BoundingRectViewportUpdate );

    setBackgroundBrush( Qt::transparent );
    setFrameStyle( QFrame::NoFrame );

    /** Palette: Make the base transparent */
    QPalette palette( this->palette() );

    palette.setColor( QPalette::Base, Qt::transparent );
    setPalette( palette );

    /** Zoom in/out with mouse as the center */
    setResizeAnchor( QGraphicsView::AnchorUnderMouse );

    fsw = new DesQ::FSWatcher();
    connect(
        fsw, &DesQ::FSWatcher::nodeChanged, [ = ]( QString path ) {
            if ( mFileName == path ) {
                emit fileModified( mFileName );
            }
        }
    );
    connect(
        fsw, &DesQ::FSWatcher::nodeDeleted, [ = ]( QString path ) {
            if ( mFileName == path ) {
                emit fileDeleted( mFileName );
            }
        }
    );

    /* Static Images */
    image = new QGraphicsPixmapItem();

    /* GIF Images */
    animImage = new QGraphicsProxyWidget();

    mScene = new QGraphicsScene();
    mScene->setSceneRect( QRectF( 0, 0, 256, 256 ) );

    setScene( mScene );

    /** Show info about image */
    nameLbl = new DesQ::Eye::ImageInfoWidget( this );
}


void DesQ::Eye::View::loadImage( QString fileName ) {
    if ( not DesQ::Utils::isFile( fileName ) ) {
        return;
    }

    /** We can simply watch this directory in PathOnly mode. It will notify us about changed images */
    fsw->addWatch( DesQ::Utils::dirName( fileName ), DesQ::FSWatcher::PathOnly );

    mFileName = fileName;

    QSize vwSize = viewport()->size();
    QSize pxSize;

    /* Remove all items */
    mScene->clear();

    /* Reset all transformations */
    resetTransform();

    /* SVG */
    if ( fileName.endsWith( ".svg" ) or fileName.endsWith( ".svgz" ) ) {
#if ( (RESVG_MAJOR_VERSION <= 0) && (RESVG_MINOR_VERSION <= 10) )
        svgPainter = new ResvgRenderer( fileName );

#else
        svgPainter = new ResvgRenderer( fileName, ResvgOptions() );

#endif

        QImage svg = svgPainter->renderToImage();

        image = new QGraphicsPixmapItem();
        image->setPixmap( QPixmap::fromImage( svg ) );

        mScene->addItem( image );
        mScene->setSceneRect( image->boundingRect() );

        pxSize = svgPainter->defaultSize();

        mType = DesQ::Eye::ImageType::VectorImage;
    }

    /* GIF */
    else if ( fileName.endsWith( ".gif" ) or fileName.endsWith( ".mng" ) ) {
        QMovie *gif     = new QMovie( fileName );
        QLabel *animLbl = new QLabel();
        animLbl->setAlignment( Qt::AlignCenter );
        animLbl->setMovie( gif );

        animImage = new QGraphicsProxyWidget();
        animImage->setWidget( animLbl );
        mScene->addItem( animImage );

        connect(
            gif, &QMovie::resized, [ = ]( QSize size ) {
                if ( animLbl->width() < size.width() ) {
                    animLbl->setFixedWidth( size.width() );
                }

                if ( animLbl->height() < size.height() ) {
                    animLbl->setFixedHeight( size.height() );
                }

                QSize imSz = animLbl->size();
                mScene->setSceneRect( animImage->boundingRect() );

                if ( (imSz.width() > vwSize.width() ) or (imSz.height() > vwSize.height() ) ) {
                    zoomFit();
                }
            }
        );

        gif->start();

        mType = DesQ::Eye::ImageType::AnimatedImage;
    }

    /* Other */
    else {
        QPixmap pix = QPixmap( fileName );

        if ( pix.isNull() ) {
            pix = QIcon::fromTheme( "image-x-generic" ).pixmap( 256 );
        }

        image = new QGraphicsPixmapItem( pix );
        image->setTransformationMode( Qt::SmoothTransformation );
        mScene->addItem( image );
        mScene->setSceneRect( image->boundingRect() );

        pxSize = pix.size();

        mType = DesQ::Eye::ImageType::StaticImage;
    }

    /** Perform a zoomFit only if this widget has been made visible */
    if ( isVisible() ) {
        if ( (pxSize.width() > vwSize.width() ) or (pxSize.height() > vwSize.height() ) ) {
            zoomFit();
        }
    }

    setSceneRect( mScene->itemsBoundingRect() );

    emit imageChanged( fileName );

    nameLbl->updateInfo( fileName );
}


void DesQ::Eye::View::clear() {
    isFit = false;
}


QImage DesQ::Eye::View::asImage() {
    return image->pixmap().toImage();
}


qreal DesQ::Eye::View::zoomFactor() {
    return mScale;
}


void DesQ::Eye::View::zoomIn() {
    /**
     * TODO
     * Find a better way to zoom in and zoom out
     * Perhaps use OpenCV image manipulation?
     */

    mScale *= scaleFactor;

    if ( mType == DesQ::Eye::ImageType::VectorImage ) {
        QSizeF newSz( sceneRect().size() * scaleFactor );
        QImage svg = svgPainter->renderToImage( newSz.toSize() );
        image->setPixmap( QPixmap::fromImage( svg ) );
        mScene->setSceneRect( image->boundingRect() );
    }

    else {
        scale( scaleFactor, scaleFactor );
    }

    isFit = false;
    zoomChanged();
}


void DesQ::Eye::View::zoomOut() {
    /**
     * TODO
     * Find a better way to zoom in and zoom out
     * Perhaps use OpenCV image manipulation?
     */

    mScale *= invScaleFactor;

    if ( mType == DesQ::Eye::ImageType::VectorImage ) {
        QSizeF newSz( sceneRect().size() * invScaleFactor );
        QImage svg = svgPainter->renderToImage( newSz.toSize() );
        image->setPixmap( QPixmap::fromImage( svg ) );
        mScene->setSceneRect( image->boundingRect() );
    }

    else {
        scale( invScaleFactor, invScaleFactor );
    }

    isFit = false;
    zoomChanged();
}


void DesQ::Eye::View::zoomNormal() {
    if ( mType == DesQ::Eye::ImageType::VectorImage ) {
        QImage svg = svgPainter->renderToImage();
        image->setPixmap( QPixmap::fromImage( svg ) );
        mScene->setSceneRect( image->boundingRect() );
    }

    else {
        resetTransform();
    }

    isFit = false;

    mScale = 1.0;
    zoomChanged();
}


void DesQ::Eye::View::zoomFit() {
    if ( mType == DesQ::Eye::ImageType::AnimatedImage ) {
        fitInView( animImage, Qt::KeepAspectRatio );
    }

    else if ( mType == DesQ::Eye::ImageType::VectorImage ) {
        QImage svg = svgPainter->renderToImage( viewport()->size() );
        image->setPixmap( QPixmap::fromImage( svg ) );
        mScene->setSceneRect( image->boundingRect() );
    }

    else {
        fitInView( image, Qt::KeepAspectRatio );
    }

    isFit = true;

    /* transform() returns the 2D scaling matrix */
    mScale = transform().m11();

    emit zoomChanged();
}


void DesQ::Eye::View::rotateLeft() {
    QTransform tran;

    tran.rotate( -90 );

    mScene->removeItem( image );
    image->setPixmap( image->pixmap().transformed( tran, Qt::SmoothTransformation ) );
    mScene->addItem( image );
    mScene->setSceneRect( image->boundingRect() );

    if ( isFit ) {
        zoomFit();
    }
}


void DesQ::Eye::View::rotateRight() {
    QTransform tran;

    tran.rotate( 90 );

    mScene->removeItem( image );
    image->setPixmap( image->pixmap().transformed( tran, Qt::SmoothTransformation ) );
    mScene->addItem( image );
    mScene->setSceneRect( image->boundingRect() );

    if ( isFit ) {
        zoomFit();
    }
}


void DesQ::Eye::View::reload() {
    image->setPixmap( QPixmap( mFileName ) );
}


bool DesQ::Eye::View::saveImage( QString fileName ) {
    if ( fileName.length() ) {
        mFileName = fileName;
    }

    return image->pixmap().save( mFileName );
}


void DesQ::Eye::View::mouseDoubleClickEvent( QMouseEvent *event ) {
    if ( event->button() == Qt::LeftButton ) {
        if ( isFit ) {
            zoomNormal();
        }

        else {
            zoomFit();
        }
    }
}


void DesQ::Eye::View::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();

    /** Showing the image for the first time */
    if ( mScale < 0 ) {
        mScale = 1.0;

        resetTransform();

        QSizeF imgSz = sceneRect().size();
        QSizeF vwSz  = viewport()->size();

        if ( (imgSz.width() > vwSz.width() ) or (imgSz.height() > vwSz.height() ) ) {
            zoomFit();
        }
    }

    else {
        if ( isFit ) {
            zoomFit();
        }
    }

    if ( mType == DesQ::Eye::ImageType::AnimatedImage ) {
        mScene->setSceneRect( animImage->boundingRect() );
    }

    else {
        mScene->setSceneRect( image->boundingRect() );
    }

    nameLbl->setFixedWidth( rEvent->size().width() );
    nameLbl->move( 0, rEvent->size().height() - 48 );
}


void DesQ::Eye::View::wheelEvent( QWheelEvent *wEvent ) {
    int angle = wEvent->angleDelta().y();

    /* Change zoom */
    if ( qApp->keyboardModifiers() & Qt::ControlModifier ) {
        if ( angle > 0 ) {
            zoomIn();
        }

        else if ( angle < 0 ) {
            zoomOut();
        }
    }

    /* Change Image */
    else if ( qApp->keyboardModifiers() & Qt::AltModifier ) {
        if ( angle > 0 ) {
            emit prevImage();
        }

        else if ( angle < 0 ) {
            emit nextImage();
        }
    }

    /* Scroll */
    else {
        QGraphicsView::wheelEvent( wEvent );
    }

    wEvent->accept();
}


void DesQ::Eye::View::keyPressEvent( QKeyEvent *kEvent ) {
    /**
     * We'll be emitting the prev/next image signals
     * only if the current image is not exceeding the
     */
    int hmax = horizontalScrollBar()->maximum();

    switch ( kEvent->key() ) {
        case Qt::Key_Left: {
            /* Previous Image only if it's not horizontally zoomed out */
            if ( not hmax ) {
                emit prevImage();
            }

            else {
                QGraphicsView::keyPressEvent( kEvent );
            }

            break;
        }

        case Qt::Key_Right: {
            /* Next Image only if it's not horizontally zoomed out */
            if ( not hmax ) {
                emit nextImage();
            }

            else {
                QGraphicsView::keyPressEvent( kEvent );
            }

            break;
        }

        default: {
            QGraphicsView::keyPressEvent( kEvent );
            break;
        }
    }
}
