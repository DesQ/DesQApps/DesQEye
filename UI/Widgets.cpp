/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <desq/Utils.hpp>
#include <DFXdg.hpp>

#include "ImageProps.hpp"
#include "Widgets.hpp"

DesQ::Eye::DeleteAlertWidget::DeleteAlertWidget( QWidget *parent ) : QWidget( parent ) {
    QLabel      *msg       = new QLabel( "This file has been deleted by an external process. Do you want to save a copy?" );
    QPushButton *saveBtn   = new QPushButton( QIcon::fromTheme( "document-save-as" ), "Save a copy" );
    QPushButton *ignoreBtn = new QPushButton( QIcon::fromTheme( "dialog-close" ), "Ignore" );

    saveBtn->setFixedHeight( 30 );
    ignoreBtn->setFixedHeight( 30 );

    connect( saveBtn, &QPushButton::clicked, this, &DesQ::Eye::DeleteAlertWidget::saveClicked );
    connect(
        ignoreBtn, &QPushButton::clicked, [ = ]() {
            emit ignoreDeletion();
            close();
        }
    );

    QHBoxLayout *baseLyt = new QHBoxLayout();

    baseLyt->addWidget( msg );
    baseLyt->addStretch();
    baseLyt->addWidget( saveBtn );
    baseLyt->addWidget( ignoreBtn );

    QWidget *base = new QWidget();

    base->setLayout( baseLyt );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( base );
    setLayout( lyt );

    setFixedHeight( 48 );

    setAttribute( Qt::WA_TranslucentBackground );
    setStyleSheet(
        "QPushButton {"
        "   border: 1px solid #DC143C;"
        "   border-radius: 3px;"
        "   background-color: rgba(220, 20, 60, 80);"
        "}"
    );
}


void DesQ::Eye::DeleteAlertWidget::saveClicked() {
    QString path = eyeSett->value( "Session/LastUsedPath" );

    if ( path.isEmpty() ) {
        path = DFL::XDG::xdgPicturesDir();
    }

    QStringList filters;

    for ( QByteArray fmt: QImageWriter::supportedImageFormats() ) {
        filters << QString( "%1 Image(*.%2)" ).arg( fmt.toUpper().data() ).arg( fmt.data() );
    }

    QString fn = QFileDialog::getSaveFileName(
        this,
        "DesQ Eye | Save Image As",
        path,
        filters.join( ";;" )
    );

    if ( not fn.isEmpty() ) {
        eyeSett->setValue( "Session/LastUsedPath", DesQ::Utils::dirName( fn ) );
        close();
        emit saveCopy( fn );
    }
}


void DesQ::Eye::DeleteAlertWidget::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );
    painter.setPen( QPen( QColor( 220, 20, 60 ), 1.0 ) );
    painter.setBrush( QColor( 220, 20, 60, 80 ) );

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 5, 5 );

    painter.end();

    QWidget::paintEvent( pEvent );
}


DesQ::Eye::ModifyAlertWidget::ModifyAlertWidget( QWidget *parent ) : QWidget( parent ) {
    setObjectName( "AlertWidget" );

    QLabel      *msg          = new QLabel( "This file has been modified by an external process. Do you want to reload it?" );
    QPushButton *overwriteBtn = new QPushButton( QIcon::fromTheme( "document-save-as" ), "Overwrite" );
    QPushButton *reloadBtn    = new QPushButton( QIcon::fromTheme( "view-refresh" ), "Reload" );
    QPushButton *ignoreBtn    = new QPushButton( QIcon::fromTheme( "dialog-close" ), "Ignore" );

    overwriteBtn->setFixedHeight( 30 );
    reloadBtn->setFixedHeight( 30 );
    ignoreBtn->setFixedHeight( 30 );

    connect(
        overwriteBtn, &QPushButton::clicked, [ = ]() {
            emit overwrite();
            close();
        }
    );
    connect(
        reloadBtn, &QPushButton::clicked, [ = ]() {
            emit reload();
            close();
        }
    );
    connect(
        ignoreBtn, &QPushButton::clicked, [ = ]() {
            emit ignoreModification();
            close();
        }
    );

    QHBoxLayout *baseLyt = new QHBoxLayout();

    baseLyt->addWidget( msg );
    baseLyt->addStretch();
    baseLyt->addWidget( overwriteBtn );
    baseLyt->addWidget( reloadBtn );
    baseLyt->addWidget( ignoreBtn );

    QWidget *base = new QWidget();

    base->setLayout( baseLyt );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( base );
    setLayout( lyt );

    setFixedHeight( 48 );
    setMinimumWidth( 600 );

    setAttribute( Qt::WA_TranslucentBackground );
    setStyleSheet(
        "QPushButton {"
        "   border: 1px solid #FFA500;"
        "   border-radius: 3px;"
        "   background-color: rgba(255, 165, 0, 80);"
        "}"
    );
}


void DesQ::Eye::ModifyAlertWidget::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing );
    painter.setPen( QPen( QColor( 255, 165, 0 ), 1.0 ) );
    painter.setBrush( QColor( 255, 165, 0, 80 ) );

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 5, 5 );

    painter.end();

    QWidget::paintEvent( pEvent );
}


DesQ::Eye::ImageInfoWidget::ImageInfoWidget( QWidget *parent ) : QLabel( parent ) {
    setFixedHeight( 48 );
    setStyleSheet( "background: rgba(0, 0, 0, 180);" );

    /** Opacity reduction timer */
    timer = new QBasicTimer();

    /** So that we get accurate mouse movement events */
    setMouseTracking( true );

    /** Opacity management */
    opacity = new QGraphicsOpacityEffect( this );
    setGraphicsEffect( opacity );
    opacity->setOpacity( 0.05 );
}


void DesQ::Eye::ImageInfoWidget::updateInfo( QString imPath ) {
    QString html = QString(
        "<table width=100%>"
        "  <tr>"
        "    <td align=left><b>%1</b></td>"
        "    <td align=right>%2</td>"
        "  </tr>"
        "  <tr>"
        "    <td align=left>%3</td>"
        "    <td align=right>%4</td>"
        "  </tr>"
        "</table>"
    );

    DesQ::Eye::ImageProps props( imPath );

    html = html.arg( props.imageFileName() ).arg( props.imageSize() ).arg( props.imagePath() ).arg( props.imageFormat() );

    setText( html );
}


void DesQ::Eye::ImageInfoWidget::enterEvent( QMouseEnterEvent *event ) {
    /** Stop the timer */
    timer->stop();

    /** Make the widget opaque */
    opacity->setOpacity( 1.0 );

    /** Force an update */
    qApp->processEvents();

    /** Let QLabel do what it does. */
    QLabel::enterEvent( event );
}


void DesQ::Eye::ImageInfoWidget::leaveEvent( QEvent *event ) {
    /** Start/Reset the timer */
    timer->start( 250, this );

    /** Let the label do what it does */
    QLabel::leaveEvent( event );
}


void DesQ::Eye::ImageInfoWidget::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == timer->timerId() ) {
        timer->stop();

        if ( not underMouse() ) {
            opacity->setOpacity( 0.05 );
        }
    }

    QLabel::timerEvent( tEvent );
}
