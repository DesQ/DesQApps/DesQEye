/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include "Widgets.hpp"

namespace DesQ {
    class FSWatcher;
}

namespace DesQ {
    namespace Eye {
        class App;
        class View;
        class ThumbsView;
        class ThumbsModel;
        class DeleteAlertWidget;
        class ModifyAlertWidget;
    }
}

class DesQ::Eye::App : public QMainWindow {
    Q_OBJECT

    public:
        App( QString path );

        /** Start the sideshow */
        void startSlideShow();

    private:
        void createUI();
        void setupConnections();
        void setWindowProperties();

        /** Toggle between full-screen and normal */
        void toggleFullScreen();

        DesQ::Eye::View *eyeView;
        DesQ::Eye::ThumbsView *thumbsView;

        QWidget *base;

        bool started     = false;
        bool shownNormal = false;

        QBasicTimer *slowResizeTimer;
        QBasicTimer *slideTimer;

        DesQ::Eye::DeleteAlertWidget *delAlert;
        DesQ::Eye::ModifyAlertWidget *modAlert;

        enum State {
            FullScreen = 0x2E63CC,
            SlideShow,
            Maximized,
            Normal
        };

        /** Easier than multiple was-bools */
        State mLastState = Normal;
        State mState     = Normal;

        /** State before fullscreen */
        QSize preFullScreenSize = QSize( 1024, 768 );

    public Q_SLOTS:
        void showFullScreen();
        void showMaximized();
        void showNormal();
        void show();

    protected:
        /** Resize Event - Hide/Show thumbsView */
        void resizeEvent( QResizeEvent * ) override;

        /** TimerEvent - To manage show/hide thumbsView */
        void timerEvent( QTimerEvent * ) override;

        /** KeyPress event - handle various key presses */
        void keyPressEvent( QKeyEvent * ) override;

        /** Transparent background */
        void paintEvent( QPaintEvent * ) override;

        /** Save the window state */
        void closeEvent( QCloseEvent * ) override;
};
