/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "DesQEye.hpp"
#include "DesQEyeView.hpp"
#include "Widgets.hpp"

#include "ThumbsView.hpp"
#include "ThumbsModel.hpp"

#include <desq/Utils.hpp>


DesQ::Eye::App::App( QString path ) : QMainWindow() {
    createUI();
    setupConnections();
    setWindowProperties();

    /** Some path is given to us */
    if ( path.isEmpty() == false ) {
        /** Given path is a folder */
        if ( DesQ::Utils::isDir( path ) ) {
            thumbsView->setRootPath( path );

            if ( eyeSett->value( "StartInFullScreen" ) ) {
                thumbsView->markAsMainView( false );
                thumbsView->firstImage();
            }

            else {
                eyeView->hide();
                thumbsView->markAsMainView( true );
                thumbsView->resize( width(), height() );
            }

            eyeView->setFocus();
            return;
        }

        /** The given path is a file */
        else if ( DesQ::Utils::isFile( path ) ) {
            eyeView->loadImage( path );
            thumbsView->setRootPath( QFileInfo( path ).absolutePath() );

            eyeView->setFocus();
            return;
        }
    }

    /** path is empty or not a file/folder: so get a file */
    do {
        path = QFileDialog::getOpenFileName(
            this,
            "DesQEye - Load Image",
            QDir::homePath(),
            QString( "Images (*." ) + DesQ::Eye::ThumbsModel::supportedFormats.join( " *." ) + ")"
        );
    } while ( path.isEmpty() );

    eyeView->loadImage( path );
    thumbsView->setRootPath( QFileInfo( path ).absolutePath() );

    eyeView->setFocus();
}


void DesQ::Eye::App::createUI() {
    thumbsView = new DesQ::Eye::ThumbsView( this );

    delAlert = new DesQ::Eye::DeleteAlertWidget( this );
    modAlert = new DesQ::Eye::ModifyAlertWidget( this );

    eyeView = new DesQ::Eye::View( this );

    QGridLayout *lyt = new QGridLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->setSpacing( 0 );

    lyt->addWidget( thumbsView, 0, 0, 3, 1 );
    lyt->addWidget( delAlert,   0, 1 );
    lyt->addWidget( modAlert,   1, 1 );
    lyt->addWidget( eyeView,    2, 1 );

    /** We'll be using this later on */
    base = new QWidget( this );

    base->setLayout( lyt );
    setCentralWidget( base );

    delAlert->hide();
    modAlert->hide();

    slowResizeTimer = new QBasicTimer();
    slideTimer      = new QBasicTimer();
}


void DesQ::Eye::App::setupConnections() {
    connect(
        thumbsView, &DesQ::Eye::ThumbsView::loadImage, [ = ]( QString fileName ) {
            /** Show the eyeView. */
            eyeView->show();

            /** Hide on fullscreen */
            if ( isFullScreen() and eyeSett->value( "HideThumbsInFullScreen" ) ) {
                thumbsView->hide();
            }

            else {
                thumbsView->markAsMainView( false );
            }

            if ( eyeView->isVisible() && thumbsView->isVisible() ) {
                thumbsView->move( 0, 0 );
                eyeView->move( 200, 0 );
                eyeView->resize( width() - thumbsView->width(), height() );
            }

            eyeView->loadImage( fileName );
        }
    );

    connect( eyeView,  &DesQ::Eye::View::nextImage,                       thumbsView, &DesQ::Eye::ThumbsView::nextImage );
    connect( eyeView,  &DesQ::Eye::View::prevImage,                       thumbsView, &DesQ::Eye::ThumbsView::previousImage );
    connect( eyeView,  &DesQ::Eye::View::imageChanged,                    thumbsView, &DesQ::Eye::ThumbsView::selectImage );

    connect( eyeView,  &DesQ::Eye::View::fileDeleted,                     delAlert,   &DesQ::Eye::DeleteAlertWidget::show );
    connect( eyeView,  &DesQ::Eye::View::fileModified,                    modAlert,   &DesQ::Eye::ModifyAlertWidget::show );

    connect( delAlert, &DesQ::Eye::DeleteAlertWidget::ignoreDeletion,     thumbsView, &DesQ::Eye::ThumbsView::nextImage );
    connect( modAlert, &DesQ::Eye::ModifyAlertWidget::ignoreModification, thumbsView, &DesQ::Eye::ThumbsView::nextImage );

    connect( delAlert, &DesQ::Eye::DeleteAlertWidget::saveCopy,           eyeView,    &DesQ::Eye::View::saveImage );
    connect(
        modAlert, &DesQ::Eye::ModifyAlertWidget::overwrite, [ = ] () {
            eyeView->saveImage();
        }
    );

    /* Zoom to actual size */
    QAction *origSizeAct = new QAction( "Original Size", this );

    origSizeAct->setShortcut( tr( "Ctrl+0" ) );
    connect( origSizeAct, &QAction::triggered, eyeView, &DesQ::Eye::View::zoomNormal );

    /* Zoom In */
    QAction *zoomInAct = new QAction( "Zoom In", this );

    zoomInAct->setShortcuts( { QKeySequence( QKeySequence::ZoomIn ), QKeySequence( Qt::Key_Plus ) } );
    connect( zoomInAct, &QAction::triggered, eyeView, &DesQ::Eye::View::zoomIn );

    /* Zoom Out */
    QAction *zoomOutAct = new QAction( "Zoom Out", this );

    zoomOutAct->setShortcuts( { QKeySequence( QKeySequence::ZoomOut ), QKeySequence( Qt::Key_Minus ) } );
    connect( zoomOutAct, &QAction::triggered, eyeView, &DesQ::Eye::View::zoomOut );

    /* Zoom to FIt */
    QAction *fitViewAct = new QAction( "Fit in View", this );

    fitViewAct->setShortcut( tr( "Ctrl+W" ) );
    connect( fitViewAct, &QAction::triggered, eyeView, &DesQ::Eye::View::zoomFit );

    /* First Image */
    QAction *firstAct = new QAction( "Load First Image", this );

    firstAct->setShortcut( tr( "Home" ) );
    connect( firstAct, &QAction::triggered, thumbsView, &DesQ::Eye::ThumbsView::firstImage );

    /* Last Image */
    QAction *lastAct = new QAction( "Load Last Image", this );

    lastAct->setShortcut( tr( "End" ) );
    connect( lastAct, &QAction::triggered, thumbsView, &DesQ::Eye::ThumbsView::lastImage );

    /* Rotate Right (Clockwise) */
    QAction *rotateCWAct = new QAction( "Rotate Right", this );

    rotateCWAct->setShortcut( tr( "Ctrl+R" ) );
    connect( rotateCWAct, &QAction::triggered, eyeView, &DesQ::Eye::View::rotateRight );

    /* Rotate Left (Counter-Clockwise) */
    QAction *rotateCCWAct = new QAction( "Rotate Left", this );

    rotateCCWAct->setShortcut( tr( "Ctrl+Shift+R" ) );
    connect( rotateCCWAct, &QAction::triggered, eyeView, &DesQ::Eye::View::rotateLeft );

    /* Toggle FullScreen */
    QAction *toggleFSAct = new QAction( "Toggle FullScreen", this );

    toggleFSAct->setShortcut( QKeySequence( Qt::Key_F11 ) );
    connect( toggleFSAct, &QAction::triggered, this, &DesQ::Eye::App::toggleFullScreen );

    /* Start SlideShow */
    QAction *slideShowAct = new QAction( "Begin SlideShow", this );

    slideShowAct->setShortcut( QKeySequence( Qt::Key_F5 ) );
    connect( slideShowAct, &QAction::triggered, this, &DesQ::Eye::App::startSlideShow );

    /* Show/hide thumbsView */
    QAction *toggleThumbsAct = new QAction( "Toggle thumbnails", this );

    toggleThumbsAct->setShortcut( QKeySequence( Qt::Key_F7 ) );
    connect(
        toggleThumbsAct, &QAction::triggered, [ = ] () {
            if ( eyeView->isVisible() ) {
                thumbsView->markAsMainView( false );

                if ( thumbsView->isVisible() ) {
                    thumbsView->hide();
                }

                else {
                    thumbsView->show();
                }
            }

            else {
                thumbsView->markAsMainView( true );
                thumbsView->show();
            }
        }
    );

    addAction( origSizeAct );
    addAction( zoomInAct );
    addAction( zoomOutAct );
    addAction( fitViewAct );
    addAction( rotateCWAct );
    addAction( rotateCCWAct );
    addAction( toggleFSAct );
    addAction( slideShowAct );
    addAction( toggleThumbsAct );

    addAction( firstAct );
    addAction( lastAct );
}


void DesQ::Eye::App::setWindowProperties() {
    setWindowTitle( "DesQ Eye" );
    setWindowIcon( QIcon( ":/DesQEye.png" ) );

    /* The last used size */
    QRect geom = eyeSett->value( "Session::Geometry" );
    preFullScreenSize = geom.size();

    if ( geom.isValid() == false ) {
        qCritical() << "Resetting to 1024x768" << preFullScreenSize;
        preFullScreenSize = QSize( 1024, 768 );
    }

    /** Resize according to previous geometry */
    resize( preFullScreenSize );

    /** We prefer a translucent background */
    setAttribute( Qt::WA_TranslucentBackground, true );
}


void DesQ::Eye::App::toggleFullScreen() {
    /** Get out of slideshow */
    if ( mState == State::SlideShow ) {
        slideTimer->stop();
        mLastState = State::SlideShow;
        mState     = State::FullScreen;

        return;
    }

    /** Get of full-screen: restore to previous state */
    if ( isFullScreen() ) {
        /** Restoring to previous state */
        if ( mLastState == State::Maximized ) {
            showMaximized();
        }

        else {
            showNormal();
        }
    }

    /** Get into full-screen */
    else {
        showFullScreen();
    }
}


void DesQ::Eye::App::startSlideShow() {
    /** If we're already in slideshow, do nothing */
    if ( mState == State::SlideShow ) {
        return;
    }

    /** Store the current state value in mLastState */
    mLastState = mState;

    /** Update current state */
    mState = State::SlideShow;

    /** Save the pre-fullscreen size: Update only if it's not maximized */
    if ( isMaximized() == false ) {
        preFullScreenSize = size();
    }

    /** Fullscreen the base */
    QMainWindow::showFullScreen();

    /** But, irrespective of all options, hide the thumbnails */
    thumbsView->hide();

    /** If the eyeView is hidden, then show a random image */
    if ( eyeView->isVisible() == false ) {
        thumbsView->nextImage();
    }

    /** SlideShow timer */
    int timeout = (int)eyeSett->value( "SlideShow::TimeOut" );

    slideTimer->start( timeout * 1000, Qt::CoarseTimer, this );
}


void DesQ::Eye::App::showFullScreen() {
    /** If we're already in full-screen, do nothing */
    if ( mState == State::FullScreen ) {
        return;
    }

    /** Store the current state value in mLastState */
    mLastState = mState;

    /** Update current state */
    mState = State::FullScreen;

    /** Save the pre-fullscreen size: Update only if it's not maximized */
    if ( isMaximized() == false ) {
        preFullScreenSize = size();
    }

    /** Show/Hide thumbnails */
    if ( eyeSett->value( "HideThumbsInFullScreen" ) ) {
        thumbsView->hide();
    }

    else {
        thumbsView->show();
    }

    /** Fullscreen the base */
    setWindowState( Qt::WindowFullScreen );
    QMainWindow::showFullScreen();

    qApp->processEvents();
}


void DesQ::Eye::App::showMaximized() {
    /** If we're already in maximized, do nothing */
    if ( mState == State::Maximized ) {
        return;
    }

    /** Store the current state value in mLastState */
    mLastState = mState;

    /** Update current state */
    mState = State::Maximized;

    /** Close the full screen view */
    close();

    thumbsView->show();
    QMainWindow::showMaximized();
}


void DesQ::Eye::App::showNormal() {
    /** The current state is Normal. Do nothing */
    if ( mState == State::Normal ) {
        return;
    }

    /** Store the current state value in mLastState */
    mLastState = mState;

    /** Update current state */
    mState = State::Normal;

    /** Exit full screen */
    close();

    /** Resize to pre-fullscreen size */
    resize( preFullScreenSize );

    /** Show normally */
    QMainWindow::showNormal();

    /* Hide the thumbsView only if some image is being shown */
    if ( (width() <= 800) && eyeView->isVisible() ) {
        thumbsView->hide();
    }

    else {
        thumbsView->show();
    }
}


void DesQ::Eye::App::show() {
    /* First time start of this window */
    if ( started == false ) {
        QMainWindow::show();

        /* User wants us to start in full screen */
        if ( eyeSett->value( "StartInFullScreen" ) ) {
            showFullScreen();
        }

        /* User wants us to start maximized, or last time it was maximized when closing */
        else if ( eyeSett->value( "StartMaximized" ) || eyeSett->value( "Session::ShowMaximized" ) ) {
            showMaximized();
        }

        /** Normal: use the last remembered size */
        else {
            showNormal();
        }

        started = true;

        return;
    }

    switch ( mLastState ) {
        case FullScreen: {
            qCritical() << "Showing as fullscreen";
            showFullScreen();
            break;
        }

        case Maximized: {
            qCritical() << "Showing as maximized";
            showMaximized();
            break;
        }

        /** By default show normal state */
        default: {
            qCritical() << "Showing normally";
            showNormal();
            break;
        }
    }
}


void DesQ::Eye::App::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();

    eyeView->setSceneRect( eyeView->scene()->itemsBoundingRect() );

    /* Restart if active: This way only the final resize event will trigger the show/hide of thumbs */
    if ( slowResizeTimer->isActive() ) {
        slowResizeTimer->stop();
    }

    slowResizeTimer->start( 50, Qt::PreciseTimer, this );
}


void DesQ::Eye::App::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == slowResizeTimer->timerId() ) {
        /* Stop the timer */
        slowResizeTimer->stop();

        /* Hide the thumbsView only if some image is being shown */
        if ( (width() <= 800) and eyeView->isVisible() ) {
            thumbsView->hide();
        }

        else {
            /** If we're fullscreen and HideThumbsInFullScreen is set, then don't show. */
            if ( (mState == State::FullScreen) && eyeSett->value( "HideThumbsInFullScreen" ) ) {
                /** Showing images */
                if ( eyeView->isVisible() ) {
                    thumbsView->hide();
                }

                /** Shows the picture gallery */
                else {
                    thumbsView->resize( width(), thumbsView->height() );
                    thumbsView->show();
                }
            }

            /** Always hide the thumbsView in SlideShows */
            else if ( (mState == State::SlideShow) ) {
                thumbsView->hide();
            }

            /** Otherwise show the thumbsView */
            else {
                thumbsView->show();
            }

            if ( eyeView->isVisible() ) {
                thumbsView->markAsMainView( false );
            }

            else {
                thumbsView->markAsMainView( true );
            }

            if ( eyeView->isVisible() && thumbsView->isVisible() ) {
                eyeView->move( 200, 0 );
                eyeView->resize( width() - thumbsView->width(), height() );
            }
        }
    }

    else if ( tEvent->timerId() == slideTimer->timerId() ) {
        thumbsView->nextImage();
    }

    else {
        QMainWindow::timerEvent( tEvent );
    }
}


void DesQ::Eye::App::keyPressEvent( QKeyEvent *kEvent ) {
    switch ( kEvent->key() ) {
        /** Play/Pause slide show */
        case Qt::Key_Space: {
            if ( slideTimer->isActive() ) {
                slideTimer->stop();
            }

            else {
                /** SlideShow timer */
                int timeout = (int)eyeSett->value( "SlideShow::TimeOut" );

                slideTimer->start( timeout * 1000, Qt::CoarseTimer, this );
            }

            break;
        }

        /** Come out of full-screen */
        case Qt::Key_F11: {
            if ( isFullScreen() ) {
                /** Stop the timer */
                slideTimer->stop();

                /** this->show() will sort the state for us */
                if ( mLastState == State::Maximized ) {
                    showMaximized();
                }

                else {
                    showNormal();
                }
            }

            /* Close app only if the user wants it so */
            else if ( eyeSett->value( "CloseOnEscape" ) ) {
                close();
            }

            break;
        }

        case Qt::Key_Escape: {
            if ( isFullScreen() ) {
                /** Stop the timer */
                slideTimer->stop();

                /** this->show() will sort the state for us */
                if ( mLastState == State::Maximized ) {
                    showMaximized();
                }

                else {
                    showNormal();
                }
            }

            /* Close app only if the user wants it so */
            else if ( eyeSett->value( "CloseOnEscape" ) ) {
                close();
            }

            break;
        }

        default: {
            QMainWindow::keyPressEvent( kEvent );
            break;
        }
    }
}


void DesQ::Eye::App::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setPen( Qt::NoPen );

    qreal  opacity = (100 - (int)eyeSett->value( "Transparency" ) ) / 100.0;
    QColor clr     = palette().color( QPalette::Base );

    clr.setAlphaF( opacity );
    painter.setBrush( clr );

    painter.drawRect( rect() );
    painter.end();

    QMainWindow::paintEvent( pEvent );
}


void DesQ::Eye::App::closeEvent( QCloseEvent *cEvent ) {
    eyeSett->setValue( "Session::ShowMaximized", isMaximized() );

    /** Save the geometry only if the window is not maximized or fullscreened */
    if ( (isMaximized() == false) && (isFullScreen() == false) ) {
        eyeSett->setValue( "Session::Geometry", geometry() );
    }

    cEvent->accept();
}
