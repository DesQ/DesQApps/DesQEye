/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
#define QMouseEnterEvent    QEvent
#else
#define QMouseEnterEvent    QEnterEvent
#endif

namespace DesQ {
    namespace Eye {
        class DeleteAlertWidget;
        class ModifyAlertWidget;
        class ImageInfoWidget;
    }
}

class DesQ::Eye::DeleteAlertWidget : public QWidget {
    Q_OBJECT;

    public:
        DeleteAlertWidget( QWidget *parent );

    private:
        void saveClicked();

    protected:
        void paintEvent( QPaintEvent * );

    Q_SIGNALS:
        void saveCopy( QString );
        void ignoreDeletion();
};

class DesQ::Eye::ModifyAlertWidget : public QWidget {
    Q_OBJECT;

    public:
        ModifyAlertWidget( QWidget *parent );

    private:
        void saveClicked();

    protected:
        void paintEvent( QPaintEvent * );

    Q_SIGNALS:
        void overwrite();
        void reload();
        void ignoreModification();
};

class DesQ::Eye::ImageInfoWidget : public QLabel {
    Q_OBJECT;

    public:
        ImageInfoWidget( QWidget *parent );

        /** Update the information */
        void updateInfo( QString );

    private:
        QBasicTimer *timer;
        QGraphicsOpacityEffect *opacity;

    protected:
        /** Increase the opacity when the mouse enters */
        void enterEvent( QMouseEnterEvent * ) override;

        /** Decrease the opacity a little while after the mouse leaves */
        void leaveEvent( QEvent * ) override;

        /** Timer to handle mouse leave events */
        void timerEvent( QTimerEvent * ) override;
};
