/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>
#include <ResvgQt.h>

namespace DesQ {
    class FSWatcher;
}

namespace DesQ {
    namespace Eye {
        enum ImageType {
            StaticImage,
            VectorImage,
            AnimatedImage
        };

        class View;
        class ImageInfoWidget;
    }
}

class DesQ::Eye::View : public QGraphicsView {
    Q_OBJECT;

    public:
        View( QWidget *parent = nullptr );

        void loadImage( QString );
        void clear();

        QImage asImage();

        qreal zoomFactor();

    public Q_SLOTS:
        /** Zoom in into the image */
        void zoomIn();

        /** Zoom out of the image */
        void zoomOut();

        /** Reset the zoom */
        void zoomNormal();

        /** Zoom to fit the image in view */
        void zoomFit();

        /** Rotate counter-clockwise */
        void rotateLeft();

        /** Rotate clockwise */
        void rotateRight();

        /** Reload the image */
        void reload();

        /** Overwrite, if the filename is not empty or otherwise, save a copy */
        bool saveImage( QString filename = QString() );

    private:
        const double scaleFactor    = 1.05;
        const double invScaleFactor = 1 / 1.05;

        bool isFit = false;
        QString mFileName;

        QGraphicsPixmapItem *image;             // Static Image
        QGraphicsProxyWidget *animImage;        // Animated Image

        QGraphicsScene *mScene;
        ResvgRenderer *svgPainter;

        DesQ::Eye::ImageInfoWidget *nameLbl;

        qreal mScale = -1.0;

        DesQ::FSWatcher *fsw;

        DesQ::Eye::ImageType mType;

    protected:

        /**
         * Mouse DoubleClick event override
         * Toggle between 1:1 and zoom to fit mode
         */
        void mouseDoubleClickEvent( QMouseEvent *event ) override;

        /**
         * Resize event override
         * Zoom to fit when resizing (only if it previously was)
         */
        void resizeEvent( QResizeEvent *rEvent ) override;

        /**
         * Wheel event override
         * Zoom/Change Image/Scroll on mouse wheel
         */
        void wheelEvent( QWheelEvent *wEvent ) override;

        /**
         * KeyPress event override
         * Go to previous/next image if the image is not zoomed
         */
        void keyPressEvent( QKeyEvent * ) override;

    Q_SIGNALS:
        void nextImage();
        void prevImage();
        void zoomChanged();

        void imageChanged( QString name );

        void fileModified( QString name );
        void fileDeleted( QString name );
};
