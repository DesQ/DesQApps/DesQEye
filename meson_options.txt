option(
    'use_qt_version',
    type: 'combo',
    choices: ['auto', 'qt5', 'qt6'],
    value: 'qt5',
    description: 'Select the Qt version to use'
)
