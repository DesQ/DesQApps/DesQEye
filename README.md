# DesQ Eye
## A simple image viewer for the DesQ Desktop Environment

DesQ Eye is a simple image viewer built on top of the powerful Qt widget toolkit, with minimal features.

Some of its features are

* Fast startup and image loading
* Simple operations like rotate and scale images
* Extensive support for custom scripts
* Fast thumbnail extraction using libexiv2
* Slideshow


### Dependencies (Debian package names):
* Qt5 (qtbase5-dev)
* Exiv2 (libexiv2-dev)
* ReSVG (libresvg-dev)
* ~~OpenCV (libopencv-core-dev, libopencv-imgproc-dev)~~
* libdesq (https://gitlab.com/DesQ/libdesq)
* libdesqui (https://gitlab.com/DesQ/libdesqui)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQApps/DesQEye.git DesQEye`
- Enter the `DesQEye` folder
  * `cd DesQEye`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Upcoming
* High Quality image rendering powered by OpenCV
* Support for custom scripts
* Use ReSVG to render SVG images.
