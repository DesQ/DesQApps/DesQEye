/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "Thumbnailer.hpp"
#include "ThumbsModel.hpp"

#include <exiv2/exiv2.hpp>
#include <exiv2/preview.hpp>
// #include <opencv2/opencv.hpp>

DesQ::Eye::Thumbnailer::Thumbnailer( QObject *parent ) : QThread( parent ) {
    fileList.clear();
    isActive = false;
}


void DesQ::Eye::Thumbnailer::acquire( QString filename ) {
    if ( not fileList.contains( filename ) ) {
        fileList << filename;
    }

    if ( not isActive ) {
        start();
    }
}


void DesQ::Eye::Thumbnailer::run() {
    while ( true ) {
        if ( fileList.count() ) {
            isActive = true;

            QString filename = fileList.takeFirst();

            if ( getThumb( filename ) ) {
                emit updateItem( filename );
            }
        }

        else {
            isActive = false;
            return;
        }
    }
}


bool DesQ::Eye::Thumbnailer::getThumb( QString item ) {
    /* Original image or exif thumbnail */
    QImage pic;

    /* Target thumbnail image */
    QPixmap thumb( QSize( 128, 128 ) );

    thumb.fill( Qt::transparent );

    try {
        /* First we will try to extract the thumbnail */
        Exiv2::Image::UniquePtr image = Exiv2::ImageFactory::open( item.toUtf8().constData() );
        image->readMetadata();

        Exiv2::ExifData exifData( image->exifData() );

        if ( not exifData.empty() ) {
            Exiv2::ExifThumbC thumbnail( exifData );
            Exiv2::DataBuf    imgBuf = thumbnail.copy();
            pic.loadFromData( imgBuf.data(), imgBuf.size() );
        }
    } catch ( ... ) {
        qDebug() << "Exiv2 cannot handle:" << item;
    }

    /* Exif Thuymbnail extraction failed. Fallback to QImageReader + down-scaling */
    if ( pic.isNull() ) {
        QImageReader picReader( item );

        pic = picReader.read();

        if ( pic.isNull() ) {
            qDebug() << "Bad image";
            return false;
        }

        /* Down-scaling to 512 using fast transformation is quite fast */
        pic = pic.scaled( 512, 512, Qt::KeepAspectRatio, Qt::FastTransformation );
    }

    /* We will have data inside pic by this time, and generally it will be larger than 128px. So down-scale
     * to 128 */
    pic = pic.scaled( 128, 128, Qt::KeepAspectRatio, Qt::SmoothTransformation );

    /* Draw a nicely centered thumbnail into @thumb */
    QPainter painter( &thumb );

    painter.drawImage( QRectF( QPointF( (128 - pic.width() ) / 2, (128 - pic.height() ) / 2 ), QSizeF( pic.size() ) ), pic );
    painter.end();

    DesQ::Eye::ThumbsModel::iconMap.insert( item, thumb );
    return true;
}
