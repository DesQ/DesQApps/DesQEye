/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>

namespace DesQ {
    namespace Eye {
        class ThumbsModel;
        class ThumbsView;
    }
}

class DesQ::Eye::ThumbsView : public QListView {
    Q_OBJECT

    public:
        ThumbsView( QWidget *parent = nullptr );

        void markAsMainView( bool );

        void setRootPath( QString );

        void firstImage();
        void lastImage();

        void nextImage();
        void previousImage();

        void selectImage( QString );

    private:
        DesQ::Eye::ThumbsModel *fsm = nullptr;
        bool mReady      = false;
        bool mIsMainView = false;

        void handleClicked( const QModelIndex& current );
        void handleChanged( const QModelIndex&, const QModelIndex& );
        void updateItem( QModelIndex );

    Q_SIGNALS:
        void loadImage( QString );

    protected:
        void resizeEvent( QResizeEvent *rEvent );
        void paintEvent( QPaintEvent * );
};
