/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>

namespace DesQ {
    namespace Eye {
        class Thumbnailer;
        class ThumbsModel;
    }
}

/**
 * The idea is simple - read the list of images in a folder - no recursion.
 * All images listed will be readable by QImageReader.
 * QMimeDatabase::mimeTypeForFile() + QMimeType::preferredSuffix()
 * Couple this with DesQ::FSWatcher - to identify added/modified/deleted/renamed files
 */

class DesQ::Eye::ThumbsModel : public QFileSystemModel {
    Q_OBJECT

    public:
        ThumbsModel( QWidget *parent );
        ~ThumbsModel();

        /* We want to ensure thumbnailing is possible for image files */
        QVariant data( const QModelIndex& idx, int role = Qt::DisplayRole ) const override;

        /* Reload the model, applying name filters, dir filters, etc */
        void reload();

        /* Place to store the image thumbnails */
        static QHash<QString, QIcon> iconMap;

        /* Supported image formats */
        static QStringList supportedFormats;

    private:
        /* Hook to obtain image thumbnails */
        DesQ::Eye::Thumbnailer *thumbnailer;
        QModelIndex mCurIdx;

    public Q_SLOTS:
        /* Once a thumbnail is generated intimate the view about it */
        void updateItem( QString filename );

    Q_SIGNALS:
        /* Signal to intimate the view about the index data change */
        void itemUpdated( QModelIndex );
};
