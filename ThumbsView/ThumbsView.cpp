/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "ThumbsView.hpp"
#include "ThumbsModel.hpp"

#include <unistd.h>

DesQ::Eye::ThumbsView::ThumbsView( QWidget *parent ) : QListView( parent ) {
    setFlow( QListView::LeftToRight );
    setViewMode( QListView::IconMode );
    setMovement( QListView::Static );
    setResizeMode( QListView::Adjust );
    setLayoutMode( QListView::SinglePass );
    setSelectionMode( QAbstractItemView::SingleSelection );

    setHorizontalScrollBarPolicy( Qt::ScrollBarAlwaysOff );
    setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

    setIconSize( QSize( 128, 128 ) );
    setGridSize( QSize( 150, 150 ) );
    setFrameStyle( QFrame::NoFrame );

    fsm = new DesQ::Eye::ThumbsModel( this );
    setModel( fsm );

    connect( this,             &QListView::clicked,                  this, &DesQ::Eye::ThumbsView::handleClicked );
    connect( selectionModel(), &QItemSelectionModel::currentChanged, this, &DesQ::Eye::ThumbsView::handleChanged );

    /** Palette: Make the base transparent */
    QPalette palette( this->palette() );

    palette.setColor( QPalette::Base, Qt::transparent );
    setPalette( palette );

    /** If the ThumbsView is ready */
    mReady = false;

    /** Connect the directoryLoaded signal to turn this flag on */
    connect(
        fsm, &QFileSystemModel::directoryLoaded, [ = ] ( QString ) mutable {
            mReady = true;
        }
    );

    /** Connect the directoryLoaded signal to turn this flag on */
    connect(
        fsm, &DesQ::Eye::ThumbsModel::itemUpdated, [ = ] () {
            repaint();
        }
    );
}


void DesQ::Eye::ThumbsView::markAsMainView( bool yes ) {
    mIsMainView = yes;

    if ( yes ) {
        setMaximumWidth( QWIDGETSIZE_MAX );
    }

    else {
        setMaximumWidth( 200 );
        resize( 200, height() );
    }
}


void DesQ::Eye::ThumbsView::setRootPath( QString path ) {
    mReady = false;
    setRootIndex( fsm->setRootPath( path ) );
}


void DesQ::Eye::ThumbsView::firstImage() {
    /** Wait until the fsm is ready */
    while ( not mReady ) {
        /** Process other events, so that the UI does not hang up. */
        qApp->processEvents();

        /** Sleep a little */
        usleep( 100 );
    }

    setCurrentIndex( fsm->index( 0, 0, rootIndex() ) );
    handleClicked( fsm->index( 0, 0, rootIndex() ) );
}


void DesQ::Eye::ThumbsView::lastImage() {
    /** Wait until the fsm is ready */
    while ( not mReady ) {
        /** Process other events, so that the UI does not hang up. */
        qApp->processEvents();

        /** Sleep a little */
        usleep( 100 );
    }

    int last = fsm->rowCount( rootIndex() ) - 1;

    setCurrentIndex( fsm->index( last, 0, rootIndex() ) );
    handleClicked( fsm->index( last, 0, rootIndex() ) );
}


void DesQ::Eye::ThumbsView::nextImage() {
    /** Wait until the fsm is ready */
    while ( not mReady ) {
        /** Process other events, so that the UI does not hang up. */
        qApp->processEvents();

        /** Sleep a little */
        usleep( 100 );
    }

    /** If we have a single or no image, do nothing */
    if ( fsm->rowCount( rootIndex() ) <= 1 ) {
        qDebug() << "No images other than the current one";
        return;
    }

    int next = currentIndex().row() + 1;

    if ( next == fsm->rowCount( rootIndex() ) ) {
        next = 0;
    }

    setCurrentIndex( fsm->index( next, 0, rootIndex() ) );
    handleClicked( fsm->index( next, 0, rootIndex() ) );
}


void DesQ::Eye::ThumbsView::previousImage() {
    /** Wait until the fsm is ready */
    while ( not mReady ) {
        /** Process other events, so that the UI does not hang up. */
        qApp->processEvents();

        /** Sleep a little */
        usleep( 100 );
    }

    /** If we have a single or no image, do nothing */
    if ( fsm->rowCount( rootIndex() ) <= 1 ) {
        return;
    }

    int previous = currentIndex().row() - 1;

    if ( previous == -1 ) {
        previous = fsm->rowCount( rootIndex() ) - 1;
    }

    setCurrentIndex( fsm->index( previous, 0, rootIndex() ) );
    handleClicked( fsm->index( previous, 0, rootIndex() ) );
}


void DesQ::Eye::ThumbsView::selectImage( QString image ) {
    setCurrentIndex( fsm->index( image ) );
}


void DesQ::Eye::ThumbsView::handleClicked( const QModelIndex& curIdx ) {
    emit loadImage( curIdx.data( Qt::UserRole + 1 ).toString() );

    markAsMainView( false );
}


void DesQ::Eye::ThumbsView::handleChanged( const QModelIndex& curIdx, const QModelIndex& oldIdx ) {
    /** If the old index is not valid only when the view is init */
    if ( not oldIdx.isValid() ) {
        return;
    }

    emit loadImage( curIdx.data( Qt::UserRole + 1 ).toString() );
    markAsMainView( false );
}


void DesQ::Eye::ThumbsView::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( viewport() );

    painter.setPen( Qt::NoPen );
    painter.setBrush( QColor( 0, 0, 0, 60 ) );
    painter.drawRect( QRectF( 0, 0, width(), height() ) );

    painter.end();

    QListView::paintEvent( pEvent );
}


void DesQ::Eye::ThumbsView::updateItem( QModelIndex idx ) {
    QAbstractItemView::dataChanged( idx, idx, QVector<int>() << Qt::DecorationRole );
}


void DesQ::Eye::ThumbsView::resizeEvent( QResizeEvent *rEvent ) {
    rEvent->accept();

    /* To prevent crashes while hiding the split view */
    int viewportWidth = qMax( viewport()->width(), 128 );

    /* Minimum item width */
    int minWidth = 180;
    int height   = 152;

    /* Items per row */
    int items = qMax( ( int )(viewportWidth / (minWidth + spacing() ) ), 1 );

    /* Minimum width of all items */
    int itemsWidth = items * minWidth;

    /* Empty space remaining */
    int empty = viewportWidth - itemsWidth;

    /* Extra space per item */
    int extra = (empty / items) - 5;

    /* New grid size */
    setGridSize( QSize( minWidth + extra, height ) );
}
