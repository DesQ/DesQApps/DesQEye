/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

// Local Headers
#include "ThumbsModel.hpp"
#include "Thumbnailer.hpp"

QStringList           DesQ::Eye::ThumbsModel::supportedFormats = QStringList();
QHash<QString, QIcon> DesQ::Eye::ThumbsModel::iconMap          = QHash<QString, QIcon>();

DesQ::Eye::ThumbsModel::ThumbsModel( QWidget *parent ) : QFileSystemModel( parent ) {
    QStringList nameFilterList;

    Q_FOREACH ( QByteArray imgFmt, QImageReader::supportedImageFormats() ) {
        supportedFormats << QString::fromLatin1( imgFmt ).toLower();
        nameFilterList << "*." + QString::fromLatin1( imgFmt ).toLower();
    }

    setFilter( QDir::Files | QDir::NoDotAndDotDot );
    setNameFilters( nameFilterList );
    setNameFilterDisables( false );

    thumbnailer = new DesQ::Eye::Thumbnailer( this );
    connect( thumbnailer, &DesQ::Eye::Thumbnailer::updateItem, this, &DesQ::Eye::ThumbsModel::updateItem );
}


DesQ::Eye::ThumbsModel::~ThumbsModel() {
    if ( thumbnailer->isRunning() ) {
        thumbnailer->terminate();
    }

    thumbnailer->deleteLater();
}


QVariant DesQ::Eye::ThumbsModel::data( const QModelIndex& idx, int role ) const {
    if ( not idx.isValid() ) {
        return QVariant();
    }

    switch ( role ) {
        case Qt::DisplayRole: {
            return QFileSystemModel::data( idx, Qt::DisplayRole ).toString();
        }

        case Qt::DecorationRole: {
            if ( idx.column() != 0 ) {
                return QIcon();
            }

            QString fn = fileName( idx );

            if ( supportedFormats.contains( fn.split( "." ).last().toLower() ) ) {
                /* If thumbnail is not acquired, acquire it */
                if ( not iconMap.contains( filePath( idx ) ) ) {
                    thumbnailer->acquire( filePath( idx ) );
                }

                /* Acquired thumbnail */
                else {
                    return iconMap.value( filePath( idx ) );
                }
            }

            return QIcon::fromTheme( "image-x-generic" );
        }

        default: {
            return QFileSystemModel::data( idx, role );
        }
    }
}


void DesQ::Eye::ThumbsModel::reload() {
    setFilter( QDir::Files | QDir::NoDotAndDotDot );
    // setNameFilters( nameFilterList );

    setRootPath( rootPath() );
}


void DesQ::Eye::ThumbsModel::updateItem( QString filename ) {
    QModelIndex idx = QFileSystemModel::index( filename );
    emit        itemUpdated( idx );
}
