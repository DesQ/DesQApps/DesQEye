/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtWidgets>

namespace DesQ {
    namespace Eye {
        class Thumbnailer;
    }
}

class DesQ::Eye::Thumbnailer : public QThread {
    Q_OBJECT

    public:
        Thumbnailer( QObject *parent );

        /* We add files here, for which thumbnail generation is needed */
        void acquire( QString filename );

    protected:
        /* Loop to generate thumbnails one by one and intimate the model */
        void run();

    private:
        /* The actual thumbnail generation takes place here */
        bool getThumb( QString item );

        /* I don't trust Qthread::isRunning(), custom flag to indicate that */
        bool isActive;

        /* List of files whose thumbnails are to be generated. */
        QStringList fileList;

    Q_SIGNALS:
        /* Signal to inform the model about thumbnail generated */
        void updateItem( QString );
};
