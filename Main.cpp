/**
 * This file is a part of DesQEye.
 * DesQEye is an Image Viewer for the DesQ Suite
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "DesQEye.hpp"

#include <QApplication>
#include <QCommandLineParser>

#include <desq/Utils.hpp>

#include <DFApplication.hpp>
#include <DFUtils.hpp>
#include <DFXdg.hpp>

DFL::Settings      *eyeSett        = nullptr;
DFL::Inotify       *fsw            = nullptr;
QStandardItemModel *imageListModel = nullptr;

int main( int argc, char *argv[] ) {
    QDir cache( DFL::XDG::xdgCacheHome() );

    DFL::log = fopen( cache.filePath( "DesQ/Eye.log" ).toLocal8Bit().data(), "a" );
    qInstallMessageHandler( DFL::Logger );

    qDebug() << "------------------------------------------------------------------------";
    qDebug() << "DesQ Eye started at" << QDateTime::currentDateTime().toString( "yyyyMMddThhmmss" ).toUtf8().constData();
    qDebug() << "------------------------------------------------------------------------";

    DFL::Application *app = new DFL::Application( argc, argv );

    app->setQuitOnLastWindowClosed( true );

    // Set application info
    app->setOrganizationName( "DesQ" );
    app->setApplicationName( "Eye" );
    app->setApplicationVersion( PROJECT_VERSION );
    app->setDesktopFileName( "desq-eye" );

    QCommandLineParser parser;

    parser.addHelpOption();
    parser.addVersionOption();

    /* Optional: Flag to start a slideshow  */
    parser.addOption( { { "slideshow", "s" }, "Start a slideshow" } );

    /* Needed */
    parser.addPositionalArgument( "image|folder", "An image file or folder containing images", "image|folder" );

    /* Process the CLI args */
    parser.process( *app );

    eyeSett = DesQ::Utils::initializeDesQSettings( "Eye", "Eye" );

    fsw = new DFL::Inotify();
    fsw->startWatch();

    imageListModel = new QStandardItemModel();

    DesQ::Eye::App *eye = nullptr;

    if ( parser.positionalArguments().count() ) {
        eye = new DesQ::Eye::App( parser.positionalArguments()[ 0 ] );
    }

    else {
        eye = new DesQ::Eye::App( QString() );
    }

    if ( parser.isSet( "slideshow" ) ) {
        eye->startSlideShow();
    }

    else {
        eye->show();
    }

    return app->exec();
}
